package com.gerardwisniewski.imageconverter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("ImageProcessUI.fxml"));

        Scene scene = new Scene(root);

        primaryStage.setMinWidth(600);
        primaryStage.setMinHeight(200);
        primaryStage.setTitle("Gerard Wisniewski Lab3");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
