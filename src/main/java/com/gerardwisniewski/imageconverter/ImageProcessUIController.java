package com.gerardwisniewski.imageconverter;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("ALL")
public class ImageProcessUIController implements Initializable {
    @FXML Button threadsButton;
    @FXML Button selectFilesButton;
    @FXML Button selectDirectoryButton;

    @FXML Label statusLabel;
    @FXML
    TableView<ImageProcessJob> imagesTableView;
    @FXML TableColumn<ImageProcessJob, String> imageNameColumn;
    @FXML TableColumn<ImageProcessJob, Double> progressColumn;
    @FXML TableColumn<ImageProcessJob, String> statusColumn;

    private File chosenDir = null;
    private ObservableList<ImageProcessJob> jobs;

    private int selectedThreadingModel = 0;
    private int customThreads = 8;
    private ExecutorService executor = Executors.newSingleThreadExecutor();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        imageNameColumn.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().getFile().getName()));
        statusColumn.setCellValueFactory(p -> p.getValue().messageProperty());
        progressColumn.setCellFactory(ProgressBarTableCell.<ImageProcessJob>forTableColumn());
        progressColumn.setCellValueFactory(p -> p.getValue().progressProperty().asObject()); // */

        jobs = FXCollections.observableList(new ArrayList<>());
        imagesTableView.setItems(jobs);
    }

    @FXML
    private void toggleThreads(ActionEvent event) {
        selectedThreadingModel = 0;
        ChoiceBox<String> threadingModel = new ChoiceBox<>(
                FXCollections.observableArrayList(
                        "Single thread",
                        "Common thread pool",
                        "Custom thread pool"
                )
        );

        threadingModel.getSelectionModel().selectFirst();
        threadingModel.setPrefWidth(300);
        threadingModel.setMinWidth(300);

        VBox box = new VBox();
        box.getChildren().add(threadingModel);
        box.setPrefWidth(320);
        box.setMinWidth(320);
        box.setPadding(new Insets(10, 10, 10, 10));

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setHeaderText("Pick the threading model");
        alert.getDialogPane().setContent(box);
        alert.showAndWait();
        selectedThreadingModel = threadingModel.getSelectionModel().getSelectedIndex();
        if(selectedThreadingModel == 2){
            TextInputDialog dialog = new TextInputDialog("8");
            dialog.setTitle("Number of threads");
            dialog.setHeaderText("Please write number of threads");
            Optional<String> result = dialog.showAndWait();
            if(result.isPresent()){
                try{
                    Integer.parseUnsignedInt(result.get());
                    customThreads = Integer.parseUnsignedInt(result.get());
                    if(customThreads == 0){
                        throw new NumberFormatException();
                    }
                }catch (NumberFormatException e){
                    selectedThreadingModel = 0;
                    alert = new Alert(AlertType.CONFIRMATION);
                    alert.setHeaderText("Wrong input.");
                    alert.showAndWait();
                }
            }
        }
        reloadExecutor();
    }

    @FXML
    private void selectSourceFiles(ActionEvent event) {
        Window stage = ((Node)event.getSource()).getScene().getWindow();
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("JPEGs", "*.jpg")
        );

        List<File> chosenFiles = chooser.showOpenMultipleDialog(stage);

        if (chosenFiles == null) return;
        else if (chosenFiles.isEmpty()) return;

        jobs.clear();
        chosenFiles.stream().forEach(file -> {
            jobs.add(new ImageProcessJob(file));
        });
    }

    @FXML
    private void selectTargetDirectory(ActionEvent event) {
        if (jobs.isEmpty()) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.setContentText("Please pick files to convert");
            alert.showAndWait();
            return;
        }

        Window stage = ((Node)event.getSource()).getScene().getWindow();
        chosenDir = new DirectoryChooser().showDialog(stage);
        if (chosenDir == null) return;
        else new Thread(this::processImages).start();
    }

    private void processImages() {
        long startTime = System.currentTimeMillis();
        Platform.runLater(() -> {
            disableButtons(true);
            statusLabel.setText("Converting...");
        });


        jobs.stream().forEach(job -> {
            job.targetDirectory = chosenDir;
            executor.submit(job);
        });

        try {
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.HOURS);

            long convDuration = System.currentTimeMillis() - startTime;
            Platform.runLater(() -> {
                statusLabel.setText("Done! Took " + convDuration + "ms.");
                disableButtons(false);
                reloadExecutor();
            });

        } catch (InterruptedException e) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.setContentText("Conversion took too long");
            alert.showAndWait();

            Platform.runLater(() -> { statusLabel.setText("ERROR"); });
        }
    }

    private void disableButtons(boolean toggle) {
        threadsButton.setDisable(toggle);
        selectFilesButton.setDisable(toggle);
        selectDirectoryButton.setDisable(toggle);
    }

    private void reloadExecutor() {
        switch (selectedThreadingModel) {
            case 0:
                threadsButton.setText("Threads: 1");
                executor = Executors.newSingleThreadExecutor();
                break;
            case 1:
                threadsButton.setText("Threads: Common Pool");
                executor = new ForkJoinPool();
                break;
            case 2:
                threadsButton.setText("Threads: "+customThreads);
                executor = Executors.newFixedThreadPool(customThreads);
                break;
        }
    }
}
