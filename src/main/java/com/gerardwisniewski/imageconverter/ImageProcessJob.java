package com.gerardwisniewski.imageconverter;

import javafx.application.Platform;
import javafx.concurrent.Task;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ImageProcessJob extends Task<Void> {
    private File sourceFile;
    File targetDirectory;

    ImageProcessJob(File file) {
        sourceFile = file;
        updateMessage("Ready");
    }

    File getFile() {
        return sourceFile;
    }

    @Override
    protected Void call() throws Exception {
        if (sourceFile == null) {
            updateMessage("SOURCE FILE IS NULL!");
            return null;
        }
        else if (targetDirectory == null) {
            updateMessage("TARGET DIRECTORY IS NULL!");
            return null;
        }
        updateMessage("Working...");
        updateProgress(0, 1);

        try {
            BufferedImage original = ImageIO.read(sourceFile);

            BufferedImage greyscale = new BufferedImage(
                    original.getWidth(),
                    original.getHeight(),
                    original.getType()
            );

            for (int i = 0; i < original.getWidth(); i++) {
                for (int j = 0; j < original.getHeight(); j++) {
                    int red = new Color(original.getRGB(i, j)).getRed();
                    int green = new Color(original.getRGB(i, j)).getGreen();
                    int blue = new Color(original.getRGB(i, j)).getBlue();
                    int luminosity = (int) (0.21 * red + 0.71 * green + 0.07 * blue);
                    int newPixel = new Color(luminosity, luminosity, luminosity).getRGB();
                    greyscale.setRGB(i, j, newPixel);
                }

                double curProgress = (1.0 + i) / original.getWidth();
                Platform.runLater(() -> updateProgress(curProgress, 1));
            }

            Path outputPath = Paths.get(
                    targetDirectory.getAbsolutePath(),
                    sourceFile.getName()
            );

            ImageIO.write(greyscale, "jpg", outputPath.toFile());
        } catch (IOException ex) {
            updateMessage("ERROR!");
            throw new RuntimeException(ex);
        }

        updateMessage("Converted");
        return null;
    }
}
